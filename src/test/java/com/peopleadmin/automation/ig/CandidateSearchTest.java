package com.peopleadmin.automation.ig;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.experimental.categories.Category;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import com.peopleadmin.automation.ig.IGCategories.*;

public class CandidateSearchTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Category({IG.class})
  @Test
  public void testCandidateSearch() throws Exception {
    driver.get("https://qa.teachermatch.org/");
    driver.findElement(By.id("emailAddress1")).sendKeys("prerana.iqa@mailinator.com");
    driver.findElement(By.id("password1")).sendKeys("password123#");
    driver.findElement(By.xpath("//button[@id='submitLogin']/i")).click();
    driver.findElement(By.id("menuid2")).click();
    driver.findElement(By.id("submenuid22")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("firstName")).sendKeys("AutotestTM1349");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Email Address'])[1]/following::button[1]")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if ("AutotestTM1349 candidate1349".equals(driver.findElement(By.id("teacherFnameAndLname1")).getText())) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    assertEquals("AutotestTM1349 candidate1349", driver.findElement(By.id("teacherFnameAndLname1")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
