package com.peopleadmin.automation.ig;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.experimental.categories.Category;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import com.peopleadmin.automation.ig.IGCategories.*;

public class SmokeTestTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Category({IG.class, IGSmokeTest.class})
  @Test
  public void testSmoke() throws Exception {
    driver.get("https://qa.teachermatch.org/");
    driver.findElement(By.id("emailAddress1")).sendKeys("prerana.iqa@mailinator.com");
    driver.findElement(By.id("password1")).sendKeys("password123#");
    driver.findElement(By.id("submitLogin")).click();
    assertTrue("",isElementPresent(By.xpath("//img[@alt='TeacherMatch Company Logo']")));
    driver.findElement(By.id("menuid1")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid11")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid11")).click();
    assertTrue("",isElementPresent(By.linkText("Candidate Details")));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid1")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid12")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid12")).click();
    assertTrue("",isElementPresent(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='School / Location Name'])[1]/following::button[1]")));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid2")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid21")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid21")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Users[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid2")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid22")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid22")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Candidate Pool[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid2")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid23")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid23")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Prospects[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid31")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid31")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Template\\(s\\)[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid32")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid32")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Competency[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid33")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid33")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage District[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid34")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid34")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Documents[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid35")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid35")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Domains[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid36")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid36")).click();
    assertTrue("",isElementPresent(By.id("searchItem")));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid37")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid37")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Events[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid38")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid38")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Groups[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid39")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid39")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Job Approval Process[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid310")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid310")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Inventory[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid311")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid311")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Job Category[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid312")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid312")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Objectives[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid313")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid313")).click();
    assertTrue("",isElementPresent(By.id("searchItem")));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid314")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid314")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*District Specific Questions Set[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid315")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid315")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*e-References Questions Set[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid316")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid316")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Job Description Library[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid317")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid317")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Onboarding Staffers[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid318")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid318")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage District[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid319")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid319")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Inventory Invitations[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid320")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid320")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Assessment Type Details[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid321")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid321")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Assessment Group[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid322")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid322")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Retake Policy[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid323")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid323")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Requisition No\\.[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid324")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid324")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage e-Reference Template[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid325")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid325")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Retake Policy History[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid326")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid326")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Assessment Message[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid327")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid327")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Alternative Assessment Group[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid328")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid328")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Recruiting Source Options[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid329")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid329")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Minimum Qualification Check Question Sets[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid330")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid330")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Minimum Qualification Check Question Pool[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid331")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid331")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage School[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid332")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid332")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Status Life Cycle[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid333")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid333")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Tags[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid334")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid334")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Virtual Video Question\\(s\\)[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid335")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid335")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage Virtual Video Question Set\\(s\\)[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid3")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid336")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid336")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Geo Zone Setup[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid4")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid41")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid41")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage District Job Orders[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid4")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid42")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid42")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Manage School Job Orders[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid5")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid51")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid51")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Ad-Hoc Reports[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid6")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid61")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid61")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Import Candidate Details[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid6")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid62")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid62")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Import Job Details[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("menuid6")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (driver.findElement(By.id("submenuid63")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("submenuid63")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue("",driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Import User Details[\\s\\S]*$"));
    for (int second = 0;; second++) {
    	if (second >= 60) fail("()");
    	try { if (!driver.findElement(By.id("loadingDiv")).isDisplayed()) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
