package com.peopleadmin.automation.ted;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.experimental.categories.Category;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import com.peopleadmin.automation.ted.TEDCategories.*;

public class EmailSubjectLineTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //REC-4387
  @Category({TED.class, TEDRecords.class, TED2018_07_24.class})
  @Test
  public void testEmailSubjectLine() throws Exception {
    driver.get("https://qa5.tedk12test.com/ondemand/Admin/Deployments/DeploymentRedirect.aspx?ri=tRdKyI8kb/noydvMWHyhn%5B%5BP%5D%5DANPa9oRkKiwXrEpKb8/0t5j255qAp4N//SQLNtTt83110Uq8gmjyerljE6AyorW07QPSINMNqNQLsLuGvNdgO1ktuhGAsfyRSyrPXoKeOS");
    driver.findElement(By.xpath("//*[@id=\"topNavConfigureLink\"]/div")).click();
    driver.findElement(By.id("ChecklistSetupLink")).click();
    driver.findElement(By.linkText("New Checklist ABC")).click();
    driver.findElement(By.id("BulkAssignButton")).click();
    driver.findElement(By.id("UserSearch")).sendKeys("TED Automation");
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if ("Automation".equals(driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Hire Date'])[1]/following::td[2]")).getText())) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Hire Date'])[1]/following::input[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[5]/following::span[1]")).click();
    driver.get("https://www.mailinator.com/");
    driver.findElement(By.id("inboxfield")).sendKeys("tedautomation12345");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Millions of Inboxes. All Yours.'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='QA #5 (Manual Testing)'])[1]/following::div[1]")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*PeopleAdmin Records - You have a new Checklist - QA #5 \\(Manual Testing\\)[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*PeopleAdmin Records - You have a new Checklist - QA #5 \\(Manual Testing\\)[\\s\\S]*$"));
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
