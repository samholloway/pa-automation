package com.peopleadmin.automation.ted;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.experimental.categories.Category;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import com.peopleadmin.automation.ted.TEDCategories.*;

public class ReportsPageDepartmentsTextTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //REC-4340
  @Category({TED.class, TEDRecords.class, TED2018_07_24.class})
  @Test
  public void testReportsPageDepartmentsText() throws Exception {
    driver.get("https://qa5.tedk12test.com/ondemand/Admin/Deployments/DeploymentRedirect.aspx?ri=tRdKyI8kb/noydvMWHyhn%5B%5BP%5D%5DANPa9oRkKiwXrEpKb8/0t5j255qAp4N//SQLNtTt83110Uq8gmjyerljE6AyorW07QPSINMNqNQLsLuGvNdgO1ktuhGAsfyRSyrPXoKeOS");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::div[9]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Export'])[1]/following::span[1]")).click();
    assertEquals("Departments", driver.findElement(By.linkText("Departments")).getText());
    driver.findElement(By.id("CustomReportLink")).click();
    driver.findElement(By.id("addStaffInfoBtn")).click();
    assertEquals("User Department Code", driver.findElement(By.xpath("//div[@id='staffInfoDialog']/label[4]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Add User Information'])[2]/following::button[1]")).click();
    driver.findElement(By.id("addFormElementsBtn")).click();
    assertEquals("User Department Code", driver.findElement(By.xpath("//div[@id='formElementsDialog']/div/label[4]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Add Form Elements'])[2]/following::button[1]")).click();
    driver.findElement(By.id("viewReportBtn")).click();
    for (int second = 0;; second++) {
      if (second >= 60) fail("timeout");
      try { if (driver.findElement(By.id("viewFiltersBtn")).isDisplayed()) break; } catch (Exception e) {}
      Thread.sleep(1000);
    }
    driver.findElement(By.id("viewFiltersBtn")).click();
    assertEquals("Departments", driver.findElement(By.linkText("Departments")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
