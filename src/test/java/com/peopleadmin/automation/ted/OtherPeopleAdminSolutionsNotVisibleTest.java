package com.peopleadmin.automation.ted;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.experimental.categories.Category;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import com.peopleadmin.automation.ted.TEDCategories.*;

public class OtherPeopleAdminSolutionsNotVisibleTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //REC-4382
  @Category({TED.class, TEDRecords.class, TED2018_07_24.class})
  @Test
  public void testOtherPeopleAdminSolutionsNotVisible() throws Exception {
    driver.get("https://qa5.tedk12test.com/ondemand/Admin/Deployments/DeploymentRedirect.aspx?ri=tRdKyI8kb/noydvMWHyhn%5B%5BP%5D%5DANPa9oRkKiwXrEpKb8/0t5j255qAp4N//SQLNtTt83110Uq8gmjyerljE6AyorW07QPSINMNqNQLsLuGvNdgO1ktuhGAsfyRSyrPXoKeOS");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[1]/following::img[1]")).click();
    // ERROR: Caught exception [Error: unknown strategy [class] for locator [class=sso-modal]]
    // Warning: assertTextNotPresent may require manual changes
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Other PeopleAdmin Solutions[\\s\\S]*$"));
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
