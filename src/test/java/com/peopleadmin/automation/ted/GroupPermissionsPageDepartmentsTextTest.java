package com.peopleadmin.automation.ted;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.experimental.categories.Category;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import com.peopleadmin.automation.ted.TEDCategories.*;

public class GroupPermissionsPageDepartmentsTextTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //REC-4337
  @Category({TED.class, TEDRecords.class, TED2018_07_24.class})
  @Test
  public void testGroupPermissionsPageDepartmentsText() throws Exception {
    driver.get("https://qa5.tedk12test.com/ondemand/Admin/Deployments/DeploymentRedirect.aspx?ri=tRdKyI8kb/noydvMWHyhn%5B%5BP%5D%5DANPa9oRkKiwXrEpKb8/0t5j255qAp4N//SQLNtTt83110Uq8gmjyerljE6AyorW07QPSINMNqNQLsLuGvNdgO1ktuhGAsfyRSyrPXoKeOS");
    driver.findElement(By.xpath("//*[@id=\"topNavConfigureLink\"]/div")).click();
    driver.findElement(By.id("SecuritySummaryLink")).click();
    assertEquals("View direct reports and selected departments only", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Testing Group'])[1]/following::td[1]")).getText());
    assertEquals("Departments", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Answer Bank'])[1]/following::td[28]")).getText());
    driver.findElement(By.id("Security_AddSecurityGroup")).click();
    assertEquals("Be able to view information for direct reports and selected departments only", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Enabled'])[1]/following::td[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
